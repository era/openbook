// Playing with concepts from: https://mattweidner.com/2022/02/10/collaborative-data-design.html

type HtmlResult = string;

class CRDTObject {

}

class EnableWinsFlag {

}

// Last Writer Wins Register
class LWWRegister<Type> {
  state: Type;
  constructor(state: Type) {
    this.state = state;
  }
}

class MovableListOfCRDTs<Type> {
  state: Type;
  constructor(state: Type) {
    this.state = state;
  }
}

class CRDTValuedMap<Key, Value> {
  state: Map<Key, Value>;
  constructor(state: Map<Key, Value>) {
    this.state = state;
  }

}

class Cell extends CRDTObject {
  content: MovableListOfCRDTs<string>;
  evaluation: LWWRegister<HtmlResult>;
  constructor(content: MovableListOfCRDTs<string>, evaluation: LWWRegister<HtmlResult>) {
    super();
    this.content = content;
    this.evaluation = evaluation;
  }
}

class Row extends CRDTObject {
  isVisible: EnableWinsFlag;
  constructor(isVisible: EnableWinsFlag) {
    super();
    this.isVisible = isVisible;
  }
}


class State {
 rows: MovableListOfCRDTs<Row>;
 cells: CRDTValuedMap<Row, Cell>;

  constructor(rows: MovableListOfCRDTs<Row>, cells: CRDTValuedMap<Row, Cell>) {
    this.rows = rows;
    this.cells = cells;
  }
}

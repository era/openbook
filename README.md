# openbook

The idea is to create a library to learn CRDT. In order to make this fun, this library will allow users to quickly create a new Notebook-like (e.g. Jupyter) system where people can type into "cells" and execute it. The execution can be anything that the user defines as a callback while setting up the system. It can for example send the data to a server to be evaluated or it can be send to a evaluator running on WebAssembly.
